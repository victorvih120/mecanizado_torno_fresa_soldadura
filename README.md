# CLASES DE MECANIZADO: TORNO, FRESA Y SOLDADURA

El presente proyecto se orienta en dos partes, la primera es sobre *mecanizado por desprendimiento de viruta* y la segunda parte como procesos de soldadura.

## A. Mecanizado por arranque de viruta 

Es un [proceso de mecanizado](https://www.kuzudecoletaje.es/mecanizado-arranque-viruta/) que consiste en separar material de una pieza fabricada previamente, normalmente por fundición, forja, laminación o por pulvimetalurgia.

El nombre de esta técnica se debe a que el material es arrancado o cortado con una herramienta dando lugar a la viruta, la misma que es considerada un desperdicio. La herramienta consta, generalmente, de uno o varios filos o cuchillas que separan la viruta de la pieza en cada pasada.

Las virutas se diferencian entre sí, dependiendo de la herramienta con que se esté mecanizando

![Mecanizado](desViruta.jpg)

### Procesos de mecanizado por arranque de viruta

En el mecanizado por arranque de viruta se dan tres tipos de procesos:

+ **Desbaste:** eliminación de mucho material con poca precisión; es un proceso intermedio que se utiliza para acercarse a las dimensiones finales de la pieza en un corto periodo de tiempo. Requiere alta velocidad de avance y de corte.

+ **Acabado:** eliminación de poco material con mucha precisión; proceso final cuyo objetivo es el de dar el acabado superficial que se requiera a las distintas superficies de la pieza. Se utiliza pensando en tener una superficie con poca rugosidad. Velocidad de avance baja y velocidades de corte altas.

+ **Rectificado o superacabado:** Se utiliza para un buen acabado superficial y medidas muy precisas. Las velocidades tanto de corte como de avance son muy altas, desprendiendo partículas por abrasión.

